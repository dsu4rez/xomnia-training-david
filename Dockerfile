FROM python:3.7

COPY . /ci-cd-training
WORKDIR /ci-cd-training

RUN python setup.py install

WORKDIR /ci-cd-training/app

EXPOSE 8080

ENTRYPOINT [ "python" ]
CMD ["main.py"]