from app.logic import do_stuff as victim


def test_do_stuff():
    expected_result = "exampleelpmaxe"
    assert victim("example") == expected_result
