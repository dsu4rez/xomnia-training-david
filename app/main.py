from flask import Flask
from logic import do_stuff

flask_app = Flask(__name__)


@flask_app.route("/", methods=['GET'])
def hello():
    return "Hello World!"


@flask_app.route("/compute/<awesome_param>", methods=['GET'])
def compute(awesome_param):
    result = do_stuff(awesome_param)
    return result


if __name__ == '__main__':
    flask_app.run(debug=True, host='0.0.0.0', port=8080)
